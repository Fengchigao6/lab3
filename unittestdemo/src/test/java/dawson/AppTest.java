package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    
    @Test
    public void test(){
        assertEquals("Test if echo() returns the correct value", 5, App.echo(5));
    }

    @Test
    public void test1(){
        assertEquals("Test if oneMore() returns the correct value", 6, App.oneMore(5));
    }
}
